![Icon](/img/SampleDB_PitchGlich.png)

# FoxDot_SampleDB_PitchGlitch

This project results in a creation of a sample database made from audio samples under Creative Common CC0 License.

It is created to be used as a sample database for FoxDot.

## Installation
It is possible to replace the original sample database of FoxDot by simply replacing it with this version.

However, it is highly recommended to use our Distribution in order to benefit from it advantages, like the Sample Chart app embedded in FoxDot, as well as the attribute "sdb", that makes it possible to jump between sample databases.

This installation guide is based on the use of our branch pitchglitch (aka killa_features).

1. Download and install our version of FoxDot

  [FoxDot Branch "PitchGlitch"](https://gitlab.com/iShapeNoise/foxdot)

2. Open FoxDot and select **Menu >> Help & Settings >> Open Samples Folder**

  <img src="/img/Install_SampleDB_1.png" alt="drawing" width="600"/>

3. Copy and paste the entire folder **1** into this ***snd*** folder. The folder **0** contains all default samples of FoxDot.

  <img src="/img/Install_SampleDB_2.png" alt="drawing" width="600"/>

  ***If you would like to build your own sample database, just create a folder with name "2" and build the same folder structure within, that you can see in the other folders.***

4. Reopen FoxDot and choose **Menu >> Help & Settings >> Open config file (advanced)**

  <img src="/img/Install_SampleDB_3.png" alt="drawing" width="600"/>

5. Change the value of SAMPLES_DB to **1** making this sample database to the new default. Press **Save Changes**.

  <img src="/img/Install_SampleDB_4.png" alt="drawing" width="300"/>

### And that's it!

Now you can open FoxDot and use it like you did before. However, you have 2 more options with our Distro:

1. You can jump between your sample databases using an attribute called ***sdb***. Just choose the number of the folder name you have given to your database. Here you can use **0** and **1** accordingly.

  <img src="/img/Use_SampleDB.png" alt="drawing" width="600"/>

2. You have a Sample Chart App, that can help you going through all samples and listen to it. Choose **Menu >> Help & Settings >> Open Samples Chart App**

  ***The code you need to use is shown in a text box. Just copy that and use it in your code.***

  <img src="/img/ChartApp_SampleDB.png" alt="drawing" width="600"/>
